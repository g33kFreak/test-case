import 'package:flutter/material.dart';
import './models/element.dart';

class detailsPage extends StatelessWidget {
  final element _el;
  detailsPage(this._el);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Hero(tag: _el.id, child: Image.network(_el.thumbUrl)),
          )
        ],
      ),
    ));
  }
}
