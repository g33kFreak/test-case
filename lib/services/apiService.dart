import 'dart:async';
import 'dart:convert';
import '../models/element.dart';
import 'package:http/http.dart' as http;

//getting list of elements from API
List<element> parseElements(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<element>((json) => element.fromJson(json)).toList();
}

Future<List<element>> fetchElements() async {
  String url =
      'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';
  final response = await http.get(url);
  if (response.statusCode == 200) {
    return parseElements(response.body);
  } else {
    throw Exception('API connection error');
  }
}
