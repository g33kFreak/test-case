import 'package:flutter/material.dart';
import 'package:test_project/services/apiService.dart';
import './widgets/elementWidget.dart';
import './models/element.dart';
import './services/apiService.dart';

void main() {
  runApp(MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    Future<List<element>> elementsFuture = fetchElements();
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: new PreferredSize(
            child: new Container(
              padding: EdgeInsets.only(bottom: 12),
              alignment: Alignment.bottomCenter,
              height: 85,
              decoration: BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                        color: Colors.black87,
                        blurRadius: 5,
                        offset: Offset(0, 3))
                  ],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50)),
                  color: Colors.blue),
              child: Text(
                "Test project",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 28,
                    fontWeight: FontWeight.w600),
              ),
            ),
            preferredSize: Size.fromHeight(85)),
        body: SingleChildScrollView(
            padding: EdgeInsets.only(top: 105),
            child: FutureBuilder<List<element>>(
                future: elementsFuture,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: getElements(snapshot.data)));
                  } else {
                    return Container(
                      height: 200,
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.3),
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(),
                    );
                  }
                })));
  }

  List<Widget> getElements(List<element> elements) {
    List<Widget> widgets = [];
    elements.forEach((value) {
      widgets.add(elementWidget(value));
    });
    return widgets;
  }
}
