import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../models/element.dart';
import '../detailsPage.dart';

class elementWidget extends StatelessWidget {
  element value;
  elementWidget(this.value);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => detailsPage(value))),
        child: Container(
          margin: EdgeInsets.only(bottom: 25),
          padding: EdgeInsets.only(left: 10, right: 10),
          width: MediaQuery.of(context).size.width * 0.9,
          height: 144,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              color: Colors.blue,
              boxShadow: [
                BoxShadow(
                    color: Colors.black87, blurRadius: 5, offset: Offset(0, 0))
              ]),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Hero(
                  tag: value.id,
                  child: Container(
                    width: 95,
                    height: 95,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(value.thumbUrl)),
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(100))),
                  )),
              Expanded(
                  child: Container(
                      width: 160,
                      padding: EdgeInsets.only(left: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            value.name,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w600),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                "Author: " + value.author,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400),
                              )),
                          Row(
                            children: [
                              ImageIcon(
                                AssetImage('assets/likeIcon.png'),
                                color: Colors.red,
                              ),
                              Text(
                                value.likes.toString(),
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              )
                            ],
                          )
                        ],
                      )))
            ],
          ),
        ));
  }
}
