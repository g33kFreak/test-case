class element {
  String id;
  String name;
  String author;
  int likes;
  String thumbUrl;

  String _apiKey =
      '/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

  element(this.id, this.name, this.author, this.likes, this.thumbUrl) {
    this.thumbUrl += _apiKey;
    if (this.name == null) {
      this.name = "No name :c";
    }
    if (this.name.length > 60) {
      this.name = this.name.substring(0, 57);
      this.name += '...';
    }
  }

  factory element.fromJson(Map<String, dynamic> json) {
    return element(json['id'], json['description'], json['user']['name'],
        json['likes'], json['urls']['regular']);
  }
}
